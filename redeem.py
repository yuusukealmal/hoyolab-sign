import requests, time, os, json
from dotenv import load_dotenv
import configparser

genshin = {
    "lang": "zh-tw"
}

starrail = {
    "lang": "zh-tw"
}

zzz = {
    "lang": "zh-tw",
}

class GAME:
    def __init__(self, name, domain, method, params):
        self.name = name
        self.domain = domain
        self.method = method
        self.params = params

GAMES = [
    GAME("genshin", "sg-hk4e-api", "GET", genshin),
    GAME("hkrpg", "public-operation-hkrpg", "POST", starrail),
    GAME("nap", "public-operation-nap", "POST", zzz)
]

BASE = "https://{}.hoyoverse.com/common/apicdkey/api/webExchangeCdkey{}"

def set_data():
    config = configparser.ConfigParser()
    config.read('./config.ini')

    games = {
        "GENSHIN": genshin,
        "STARRAIL": starrail,
        "ZZZ": zzz
    }
    for key, value in games.items():
        value["uid"] = config[key]["uid"]
        value["game_biz"] = config[key]["game_biz"]
        value["region"] = config[key]["region"]

def get_redeem_codes(game: GAME):
    r = requests.get("https://hoyo-codes.seria.moe/codes?game={}".format(game.name)).json()
    return[i["code"] for i in r["codes"]]

def redeem(game: GAME, code: str):
    cookies = {
        "account_mid_v2": os.environ.get('account_mid_v2'),
        "cookie_token_v2": os.environ.get('cookie_token_v2'),
    }

    game.params["cdkey"] = code
    game.params["time"] = int(time.time() * 1000)
    r = requests.request(game.method, BASE.format(game.domain, "Risk" if game.method == "POST" else ""), params=game.params, cookies=cookies)

    j = json.load(open("redeem.json", "r"))
    j[game.name].append(code)
    json.dump(j, open("redeem.json", "w"), indent=4)

    print(game.name, code, r.json())
    time.sleep(5)

def main():
    load_dotenv()
    set_data()
    for game in GAMES:
        codes = get_redeem_codes(game)
        for code in codes:
            j = json.load(open("redeem.json", "r"))
            if code not in j[game.name]:
                redeem(game, code)

if __name__ == "__main__":
    main()